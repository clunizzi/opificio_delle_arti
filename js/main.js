$(window).load(function() {
    

    $('body').addClass('loaded');
    $('.container').css({'height': '100%'});
    var body_height = $(window).height();
    $('body').css({'height': body_height + 'px'});

    
    
        var tmax_tl           = new TimelineMax(),
        polylion_shapes   = $('svg.svg-logo > g polygon'),
        polylion_stagger  = 0.00475,
        polylion_duration = 3.5;
    
    var polylion_staggerFrom = {
            scale: 0,
            opacity: 0,
            transformOrigin: 'center center',
    };

    var polylion_staggerTo = {
            opacity: 1,
            scale: 1,
            ease: Elastic.easeInOut
    };

    tmax_tl.staggerFromTo(polylion_shapes, polylion_duration, polylion_staggerFrom, polylion_staggerTo, polylion_stagger, 0);
    
    
});

$(document).ready(function() {
   


    if ($(window).width() < 768) {
    
        $('.hamburger').click(function() {
                $('nav').slideToggle("fast");
        });
    
        $('li').click(function() {
                $('nav').slideToggle("fast");
        });
    }
    
    (function($) {
        /* AleHeight = Altezza di apertura */
        AleHeight = $(window).height() * 0.9;
        
        /*AndreHeight = Altezza di chiusura */
        AndreHeight = $(window).height() * 0.7;
        $(window).scroll(function(){                          
            if ($(this).scrollTop() > AleHeight) {
                $('.nav-wrapper').slideDown(500);
            } else if ($(this).scrollTop() < AndreHeight) {
                $('.nav-wrapper').slideUp(500);
            }
        });
    
    })(jQuery);
    
    (function($) {
        
         var allowResize = true;
        
        var startTitleOffsetRdl = $(window).height() * 0.582222222;
        var startTitleOffsetFff = $(window).height() * 2.07222222;
        var startTitleOffsetCbm = ($(window).height() * 4.3671) * 0.8;
        var startTitleOffsetPdr = ($(window).height() * 5.8228) * 0.85;
         
        $(window).scroll(function() {
            
            
            allowResize = false;
            clearTimeout( $.data( this, 'scrollCheck' ) );
            $.data( this, 'scrollCheck', setTimeout(function() {
                    allowResize = true;
            }, 500) );
            
            if ( $(this).scrollTop() > startTitleOffsetRdl) {
                
                var scrollRatioRdl = (($(window).height() * 1.455555556) / $(window).width());
                var newTranslateRdl = ($(this).scrollTop() - startTitleOffsetRdl) / ((($(window).width() * scrollRatioRdl) - startTitleOffsetRdl) / 100 );
                 console.log("scrolltop:  " + $(this).scrollTop() + "   startTitleOffsetRdl: "  + startTitleOffsetRdl + "   Newtranslate RDL: " + newTranslateRdl);
              //  / ((($(window).width() * 0.909722222) - startTitleOffsetFff) / 100 )
                
                $('.rdl .titles-c-wrapper').css({
                            '-webkit-transform': 'translate(-' + newTranslateRdl + '%, -50%)',
                            '-moz-transform': 'translate(-' + newTranslateRdl + '%, -50%)',
                            '-ms-transform': 'translate(-' + newTranslateRdl + '%, -50%)',
                            '-o-transform': 'translate(-' + newTranslateRdl + '%, -50%)',
                            'transform': 'translate(-' + newTranslateRdl + '%, -50%)'
                });
            }
            
            if ( $(this).scrollTop() > startTitleOffsetFff ) {
                
                
                var scrollRatioFff = (($(window).height() * 2.911111111) / $(window).width());
                var newTranslateFff = ($(this).scrollTop() - startTitleOffsetFff) / ((($(window).width() * scrollRatioFff) - startTitleOffsetFff) / 100 );
               
                console.log("ScrollTop: "+ $(this).scrollTop() + "    Startitlefff: " + startTitleOffsetFff + "    new translate: " + newTranslateFff);
           
                $('.fff .titles-c-wrapper').css({
                            '-webkit-transform': 'translate(-' + newTranslateFff + '%, -50%)',
                            '-moz-transform': 'translate(-' + newTranslateFff + '%, -50%)',
                            '-ms-transform': 'translate(-' + newTranslateFff + '%, -50%)',
                            '-o-transform': 'translate(-' + newTranslateFff + '%, -50%)',
                            'transform': 'translate(-' + newTranslateFff + '%, -50%)'
                });
                 
            }
            
            if ( $(this).scrollTop() > startTitleOffsetCbm) {
                
                var scrollRatioCbm = (($(window).height() * 4.366666667) / $(window).width());
                var newTranslateCbm = ($(this).scrollTop() - startTitleOffsetCbm) / ((($(window).width() * scrollRatioCbm) - startTitleOffsetCbm) / 100 );
           
                $('.cbm .titles-c-wrapper').css({
                            '-webkit-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            '-moz-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            '-ms-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            '-o-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            'transform': 'translate(-' + newTranslateCbm + '%, -50%)'
                });
            
            
            }
            
             if ( $(this).scrollTop() > startTitleOffsetPdr) {
                 
                var scrollRatioPdr = (($(window).height() * 5.822222222) / $(window).width());
                var newTranslateCbm = ($(this).scrollTop() - startTitleOffsetPdr) / ((($(window).width() * scrollRatioPdr ) - startTitleOffsetPdr) / 100 );
           
                $('.pdr .titles-c-wrapper').css({
                            '-webkit-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            '-moz-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            '-ms-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            '-o-transform': 'translate(-' + newTranslateCbm + '%, -50%)',
                            'transform': 'translate(-' + newTranslateCbm + '%, -50%)'
                });
            
            
            }
            
           
        });      
    })(jQuery);
    
});
